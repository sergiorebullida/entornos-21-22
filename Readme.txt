	Trabajo en repositorio local
git init
git status
git add [nombre_fichero_o_directorio]
git commit -m "Descripción del commit"

	Sincronización repositorio local - remoto
git push [dirección_repositorio_remoto] master
git pull [repositorio_remoto] master
git clone [repositorio_remoto]

	Gestión de repositorios remotos
git remote add [alias] [dirección_remoto]
git remote add origin [dirección_remoto]
git remote
git remote -v

	Obtener versiones anteriores
git log
git checkout 3dee7c5

	Borrar archivos de las versiones
git rm [fichero]
git rm -r [directorio con archivos]
git commit -m "Archivos borrados"

	Trabajar con ramas
git branch develop
git checkout develop
git checkout master